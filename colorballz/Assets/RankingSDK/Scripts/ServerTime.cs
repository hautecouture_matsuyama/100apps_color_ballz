﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Times
{
    public double st;
}

public class ServerTime : MonoBehaviour {

    public static DateTime UnixTimeToDateTime(double unixTime)
    {
        DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
        dt = dt.AddSeconds(unixTime);
        dt = dt.AddHours(9);
        return dt;
    }

    public IEnumerator GetTime()
    {
        string url = "https://ntp-a1.nict.go.jp/cgi-bin/json";
        WWW www = new WWW(url);
        yield return www;
        if (www.error == null)
        {
            Times times = JsonUtility.FromJson<Times>(www.text);
            DateTime nowtime = UnixTimeToDateTime(times.st);
            PrefsManager.SetNowTime(nowtime.ToString());
        }
        else
        {
            DateTime nowtime = System.DateTime.Now;
            PrefsManager.SetNowTime(nowtime.ToString());
        }

        CheckSeverTime();
    }

    public void CheckSeverTime()
    {
        string initializedDay = PrefsManager.GetInitializedDay();
        string now = PrefsManager.GetNowTime();
        DateTime nowtime = DateTime.Parse(now);

        //初回プレイorランキングアップデート後初めてのプレイ
        if (string.IsNullOrEmpty(initializedDay))
        {
            //月曜なら初期化
            if (nowtime.DayOfWeek == DayOfWeek.Monday)
            {
                PrefsManager.SetWeeklyBestScore("0");
                PrefsManager.SetInitializedDay(now);
            }
        }
        else
        {
            DateTime initDay = DateTime.Parse(initializedDay);
            //月曜なら初期化
            if (nowtime.DayOfWeek == DayOfWeek.Monday && initDay < nowtime)
            {
                PrefsManager.SetWeeklyBestScore("0");
                PrefsManager.SetInitializedDay(now);
            }
        }
    }
}