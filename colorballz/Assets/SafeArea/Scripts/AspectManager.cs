﻿
//  2019/7/31   作成：兵頭
//  iPhone5のアスぺ比を基準にスケール調整する場合や、
//  使用端末のアスペ比がどれぐらいか求めるもの。

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.iOS;
using UnityEngine.UI;

public class AspectManager : MonoBehaviour {

    public static AspectManager Instance { get; private set; }

    [SerializeField]
    float aspect;

    [SerializeField]
    float fiveRate = 0.43661972f;   //iPhone5基準の縦横倍率。正確には1までの差分

    [SerializeField]
    CanvasScaler canvasscaler;

    public float rate;

    void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);

            if (Screen.width < Screen.height)
                aspect = (float)Screen.width / (float)Screen.height;
            else
                aspect = (float)Screen.height / (float)Screen.width;

            Debug.Log("縦横倍率：" + aspect);

            rate = Screen.width / canvasscaler.referenceResolution.x;
        }
    }

    public float GetAspectRate(bool fiveCompare = true)
    {
        float deviceAspect = aspect;

        //trueの場合、iPhone5の倍率を足して1より小さければ縦長の端末になる。iPadなどは1を超える計算。
        if (fiveCompare)
            deviceAspect += fiveRate;

        return deviceAspect;
    }

    public Vector2 GetPixel()
    {
        return canvasscaler.referenceResolution;
    }
}
