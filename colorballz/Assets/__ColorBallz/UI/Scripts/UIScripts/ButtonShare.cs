﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SocialConnector;
using DG.Tweening;
using System.IO;

public class ButtonShare : MonoBehaviour {

    [SerializeField]
    private Button BtnShare;
    [SerializeField]
    private Button BtnShrink;

    [SerializeField]
    private Transform obj;
    [SerializeField]
    private RawImage rawImage;
    [SerializeField]
    private RawImage BtnImage;
    [SerializeField]
    private RawImage bg;
    [SerializeField]
    private Texture[] tex;

    [SerializeField]
    private GameObject scoreText;

    private Vector3 movePosition;

    static private bool first = false;
    private float rate;

#if UNITY_ANDROID
    string url = "https://play.google.com/store/apps/developer?id=Hautecouture+Inc.";
#elif UNITY_IOS
    string url = "https://itunes.apple.com/jp/developer/hautecouture-inc./id901721933";
#endif

    string imagePath
    {
        get
        {
#if UNITY_IOS || UNITY_ANDROID
#if !UNITY_EDITOR
            return Application.persistentDataPath + "/image.png";
#else
            return "Assets/__ColorBallz/Images/image.png";
#endif
#endif
        }
    }

    [RuntimeInitializeOnLoadMethod]
    static void initialize()
    {
        first = true;
    }

    void Start()
    {
        scoreText.SetActive(false);
        movePosition = this.transform.localPosition;
        movePosition.y -= 100;
        if (first)
        {
            first = false;
            return;
        }

        else
        {
#if UNITY_IOS || UNITY_ANDROID
            //スクショデータがあるなら貼り付ける
            ShowImage();
#endif


            obj.DOScale(Vector3.one, 0.8f).SetDelay(0.3f).SetEase(Ease.OutBack).OnComplete(()=>
            {
                BtnShare.onClick.AddListener(Expansion);
            });
            first = false;
        }
    }

    public void Share()
    {
#if UNITY_EDITOR
        Debug.Log("UnityEditor上では共有できません");
        return;
#else
        BtnShrink.onClick.RemoveAllListeners();
        BtnShare.onClick.RemoveAllListeners();
        Close();
        SocialConnector.SocialConnector.Share("Get it here for free!\n#ENERGYBALL\n", url, imagePath);
#endif
    }

    public void Capture()
    {
        scoreText.SetActive(true);
#if UNITY_IOS || UNITY_ANDROID
#if !UNITY_EDITOR
        Application.CaptureScreenshot("image.png");
#else
        //Application.CaptureScreenshot("Assets/__ColorBallz/Images/image.png");
        return;
#endif
#endif
    }

    public void ShowImage()
    {
        float width = Screen.width;
        float height = Screen.height;

        rate = width / height;
        float w = 140 * rate;

        byte[] bytes = File.ReadAllBytes(imagePath);
        Texture2D texture = new Texture2D((int)w, 140);
        texture.filterMode = FilterMode.Trilinear;
        texture.LoadImage(bytes);
        rawImage.texture = texture;
        rawImage.SetNativeSize();
        rawImage.GetComponent<RectTransform>().sizeDelta = new Vector2(w, 140);
    }

    public void Close()
    {
        obj.DOScale(Vector3.zero, 0.2f).SetEase(Ease.InBack);
    }

    public void Expansion()
    {
        BtnShare.onClick.RemoveListener(Expansion);
        //拡大と一緒に角度を0度にする。
        Sequence sequence = DOTween.Sequence();

        sequence.Join(bg.DOFade(0.7f,0.2f));
        sequence.Join(BtnImage.GetComponent<RectTransform>().DOSizeDelta(new Vector2(500, 800), 0.3f));
        sequence.Join(rawImage.GetComponent<RectTransform>().DOSizeDelta(new Vector2(800 * rate, 800), 0.3f));
        sequence.Join(obj.DORotate(new Vector3(0, 0, 0), 0.3f));
        sequence.Join(obj.DOLocalMove(-movePosition, 0.3f).OnComplete(() =>
        {
            BtnShare.onClick.AddListener(Share);
            BtnShrink.onClick.AddListener(Shrink);
            BtnShrink.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }).SetRelative());

        BtnImage.texture = tex[1];

        sequence.Play();
    }

    public void Shrink()
    {
        BtnShare.onClick.RemoveListener(Share);
        BtnShrink.GetComponent<CanvasGroup>().blocksRaycasts = false;
        //拡大前まで戻す
        Sequence sequence = DOTween.Sequence();

        sequence.Join(bg.DOFade(0f, 0.2f));
        sequence.Join(BtnImage.GetComponent<RectTransform>().DOSizeDelta(new Vector2(140, 168), 0.3f));
        sequence.Join(rawImage.GetComponent<RectTransform>().DOSizeDelta(new Vector2(140 * rate, 140), 0.3f));
        sequence.Join(obj.DORotate(new Vector3(0, 0, 10), 0.3f));
        sequence.Join(obj.DOLocalMove(movePosition, 0.3f).OnComplete(() =>
        {
            BtnShare.onClick.AddListener(Expansion);
            BtnShrink.onClick.RemoveListener(Shrink);
        }).SetRelative());

        BtnImage.texture = tex[0];

        sequence.Play();
    }
}
