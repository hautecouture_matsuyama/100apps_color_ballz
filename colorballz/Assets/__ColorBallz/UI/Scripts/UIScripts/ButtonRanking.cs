﻿using UnityEngine;
using System.Collections;

namespace AppAdvisory.UI
{
   
	/// <summary>
	/// Attached to rate button
	/// </summary>
	public class ButtonRanking : MonoBehaviour 
	{
        [SerializeField]
        GameObject rankingCanvas;

        public void OpenRanking()
        {
            RankingManager.Instance.GetRankingData();
            rankingCanvas.SetActive(true);
        }
	}
}