﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
//  新規OSが追加された場合はこのスクリプトをScriptableObjectに変えてAssetファイルを作るところから始める。
//

public class Safe : MonoBehaviour{


    public IOSDevice[] devices;

    [System.Serializable]
    public struct IOSDevice{ 

        public string deviceName;
        public Vector2 rectVertical;
        public Vector2 rectHorizontal;
        public int retina;
        public Direction safeVertical;
        public Direction safeHorizontal;

    }

    [System.Serializable]
    public struct Direction
    {
        public int top;
        public int bottom;
        public int left;
        public int right;
    }
}
