﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CloseButton : MonoBehaviour {

    public GameObject CloseObject;

    public void Close()
    {
        CloseObject.SetActive(false);
    }
}
