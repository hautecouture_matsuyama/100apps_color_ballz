﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
/// <summary>
/// Attached to the Background GameObject in hierarchy, child of the GameManager GameObject.
/// In charge to fit the background size at start.
/// In charge to change the color of the background.
/// </summary>
public class Background : MonoBehaviour
{
	/// <summary>
	/// Copy the list of colors from the GameManager.
	/// </summary>
	List<Color> colors;
	/// <summary>
	/// Reference to the SpriteRenderer we will blink, child of the Background GameObject.
	/// </summary>
	public SpriteRenderer srToBlink;
	/// <summary>
	/// Save the original color of the background we defined in the editor.
	/// </summary>
	Color originalColor;
	float alpha = 255f;
	/// <summary>
	/// Initialization.
	/// </summary>
	void Awake()
	{
		colors = new List<Color>();
		var cs = FindObjectOfType<GameManager>().colors;
		foreach(Color co in cs)
		{
			colors.Add(co);
		}

		colors.Shuffle();
		Color c = GetComponent<SpriteRenderer>().color;
		c.a = alpha;
		srToBlink.color = c;
		originalColor = c;
	}
	/// <summary>
	/// Set the size and start the blink.
	/// </summary>
	void Start()
	{
		SetSize();
		DOBlink();

	}
	/// <summary>
	/// Set the size.
	/// </summary>
	void SetSize()
	{
		float width = Util.GetCamWidth();
		float height = Util.GetCamHeight();

		transform.localScale = Vector2.one * Mathf.Min(width,height);
	}
	/// <summary>
	/// Start the blink.
	/// </summary>
	void DOBlink()
	{
		colors.Shuffle();

		Color c = colors[0];
		c.a = alpha;
		srToBlink.DOColor(c,0.08f).OnComplete(() => {
			srToBlink.DOColor(originalColor,0.08f).OnComplete(() => {
				DOBlink();
			});
		});
	}

}
