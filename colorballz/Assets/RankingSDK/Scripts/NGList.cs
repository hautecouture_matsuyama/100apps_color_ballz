﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class NGList : MonoBehaviour{

    public string[] nameList;

    public bool NGCheck(string playerName)
    {
        TextAsset nameTextFile = Resources.Load("Text/ngWord", typeof(TextAsset)) as TextAsset;
        nameList = nameTextFile.text.Split(","[0]);

        for(int i = 0; i < nameList.Length; i++)
        {
            if (playerName.Contains(nameList[i]))
                return false;
        }
        return true;
    }

}
