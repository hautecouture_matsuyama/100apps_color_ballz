﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class in charge to manage input touch and desktop input in the game
/// </summary>
public class InputTouch : MonoBehaviour
{
	public delegate void TouchScreen();
	public static event TouchScreen OnTouchScreen;

	#if UNITY_TVOS
	private Vector2 startPosition;
	void OnEnable()
	{
		GameManager.OnGameStart += OnGameStart;

		GameManager.OnGameOver += OnGameOver;
	}
	void OnDisable()
	{
		GameManager.OnGameStart -= OnGameStart;

		GameManager.OnGameOver -= OnGameOver;
	}

	bool gameStarted = true;

	void OnGameStart()
	{
		UnityEngine.Apple.TV.Remote.touchesEnabled = true;
		UnityEngine.Apple.TV.Remote.allowExitToHome = false;

		gameStarted = true;
	}

	void OnGameOver()
	{
		UnityEngine.Apple.TV.Remote.touchesEnabled = false;
		UnityEngine.Apple.TV.Remote.allowExitToHome = true;

		gameStarted = false;
	}

	void Start()
	{
		UnityEngine.Apple.TV.Remote.reportAbsoluteDpadValues = true;
	}


	#endif

	void Update () 
	{
		


		#if UNITY_TVOS

		if(!gameStarted)
		{
			return;
		}

		float h = Input.GetAxis("Horizontal");

		if(h < 0)
		{
			_OnTouchLeft();
		}
		else if(h > 0)
		{
			_OnTouchRight();
		}

		int nbTouches = Input.touchCount;

		if(nbTouches > 0)
		{

			Touch touch = Input.GetTouch(0);

			TouchPhase phase = touch.phase;

			print("" + phase.ToString() + " position = " + touch.position);

		}

		#else

		if(!Application.isEditor)
		{
			if(Application.isMobilePlatform)
			{
				int nbTouches = Input.touchCount;

				if(nbTouches > 0)
				{

					Touch touch = Input.GetTouch(0);

					TouchPhase phase = touch.phase;

					if (phase == TouchPhase.Began)
					{
						if(OnTouchScreen != null)
							OnTouchScreen();
					}
				}
			}
			else
			{
				if(Input.GetMouseButtonDown(0) || Input.anyKeyDown)
				{
					if(OnTouchScreen != null)
						OnTouchScreen();
				}
			}
		}
		else
		{
			if(Input.GetMouseButtonDown(0) || Input.anyKeyDown)
			{
				if(OnTouchScreen != null)
					OnTouchScreen();
			}
		}

		#endif
	}
}
