﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using NendUnityPlugin.AD;
using GameAnalyticsSDK;

/// <summary>
/// Attached to the GameManager GameObject in hierarchy. d
/// In Charge of the logic of the game: Start, Game Over, earn 1 point, colors, sound etc...
/// In charge to send point to leaderboard using Very Simple Leaderboard : http://u3d.as/qxf
/// In charge to show interstitial using Very Simple Ads : http://u3d.as/oWD
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Number of play to show an interstitial, you need Very Simple Ads to show interstitial in this game and start monetizing it: http://u3d.as/oWD
    /// </summary>
    public int numberOfPlayToShowInterstitial = 5;
	/// <summary>
	/// The 4 colors we use in the game.
	/// </summary>
	public List<Color> colors;
	/// <summary>
	/// Reference to the background.
	/// </summary>
	public Transform background;
	/// <summary>
	/// Reference to the obstacle prefab.
	/// </summary>
	public Transform obtaclePrefab;
	/// <summary>
	/// Reference to the Player.
	/// </summary>
	Player player;
	/// <summary>
	/// Sound jump, modify it to change it.
	/// </summary>
	public AudioClip soundJump;
	/// <summary>
	/// Sound fail, modify it to change it.
	/// </summary>
	public AudioClip soundFail;
    /// <summary>
    /// Sound button, modify it to change it.
    /// </summary>
    public AudioClip soundButton;
    /// <summary>
    /// Sound point, modify it to change it.
    /// </summary>
    public AudioClip soundPoint;
    /// <summary>
    /// Sound music, modify it to change it.
    /// </summary>
    public AudioClip soundBreak;
	/// <summary>
	/// Sound music, modify it to change it.
	/// </summary>
	public AudioClip MUSIC;
	/// <summary>
	/// Reference to the audiosource we use to play sound in the game.
	/// </summary>
	AudioSource audioSource;
	/// <summary>
	/// If yes, the game is over. We use it in some methods to check if the game is over or not.
	/// </summary>
	bool isGameOver = false;
	/// <summary>
	/// Play the jump sound.
	/// </summary>
	public void PlaySoundJump()
	{
		audioSource.PlayOneShot(soundJump);
	}
	/// <summary>
	/// Play the fail sound.
	/// </summary>
	public void PlaySoundFail()
	{
		audioSource.PlayOneShot(soundFail);
	}
    /// <summary>
    /// Play the button sound.
    /// </summary>
    public void PlaySoundButton()
    {
        audioSource.PlayOneShot(soundButton);
    }
    /// <summary>
    /// Play the point sound when the player earns 1 point.
    /// </summary>
    public void PlaySoundPoint()
	{
		audioSource.PlayOneShot(soundPoint);
	}
    public void PlaySoundBreak()
    {
        audioSource.PlayOneShot(soundBreak);
    }
	/// <summary>
	/// Reference to UI Text element pointText where we display the point during the game.
	/// </summary>
	public Text pointText;
	/// <summary>
	/// Reference to UI Text element pointText where we display the last score when we are in the UI Menu.
	/// </summary>
	public Text textLast;
	/// <summary>
	/// Reference to UI Text element pointText where we display the best score when we are in the UI Menu.
	/// </summary>
	public Text textBest;
	/// <summary>
	/// The current score of the player.
	/// </summary>
	public int m_point;
	/// <summary>
	/// The current score of the player.
	/// </summary>
	int point
	{
		get
		{
			return m_point;
		}
		set
		{
			colors.Shuffle();

			m_point = value;

			pointText.text = value.ToString();

			if(value > 0)
			{
				player.DOPointAnim();
				player.DOColor(colors);
				PlaySoundPoint();
			}
		}
	}

    [SerializeField]
    private GameObject shield;

    [SerializeField]
    private ParticleSystem particle;

    static bool unbeatable = false;

    public bool Unbeatable
    {
        get
        {
            return unbeatable;
        }
        set
        {
            shield.SetActive(value);
            unbeatable = value;
        }
    }

    //アプリ起動時に呼ばれる。
    [RuntimeInitializeOnLoadMethod]
    static void Initialize()
    {
        GameAnalytics.Initialize();

        //unbeatable = true;
        if (NendAdInterstitial.Instance != null)
        {
#if UNITY_IOS
            NendAdInterstitial.Instance.Load("40f3d332a9d6be7e6da54761e67e3fc9792f5278", "650086");
#elif UNITY_ANDROID
            NendAdInterstitial.Instance.Load("e354adc070398f830a481d8db0e9ce8dfa89403d", "650083");
#endif
        }
    }
    /// <summary>
    /// Some initialization.
    /// </summary>
    void Awake()
	{
        Unbeatable = false;
        player = FindObjectOfType<Player>();

		player.gameObject.SetActive(false);

		audioSource = GetComponent<AudioSource>();

		if(Time.realtimeSinceStartup < 2)
		{
			DOTween.Init();
		}

		point = 0;

		Physics2D.gravity = new Vector2(0,-20);

		//FindObjectOfType<UIController>().DOAnimIN();

		pointText.gameObject.SetActive(false);

		textLast.text = "Last\n" + Util.GetLastScore().ToString();
		textBest.text = "Best\n" + Util.GetBestScore().ToString();
        if (PlayerPrefs.GetInt("FLG") == 1)
        {
            if(RankingManager.Instance != null)
                RankingManager.Instance.GetRankingData();
        }
    }

    /// <summary>
    /// Called by AppAdvisory.UI.UIController. We will launch the music, and do an animation to move the Player (= the ball) inside the game and we start spawning the Obstacle. 
    /// We will add a listener for a touch just in case the player touch the screen during the intro animation, to stop the animation and let the user "bounce".
    /// </summary>
    public void DOStart()
	{
		audioSource.clip = MUSIC;
		audioSource.loop = true;
		audioSource.Play();

        background.gameObject.SetActive(true);
		player.gameObject.SetActive(true);

		player.DOColor(colors);

		player.transform.position = new Vector2(-Util.GetCamWidth() / 1.5f, Util.GetCamWidth() / 2.2f);

		player.transform.DOMoveX(-Util.GetCamWidth() / 4f, 0.3f).OnComplete( () => {
			player.DOStart();
            player.transform.DOKill();
		});

		Color c = pointText.color;
		c.a = 0;
		pointText.color = c;
		pointText.gameObject.SetActive(true);
		pointText.DOFade(1,1);

		Invoke("SpawnObstacle",1);

    }
	/// <summary>
	/// Spawn a new Obstacle.
	/// </summary>
	public void SpawnObstacle()
	{
		if(isGameOver)
			return;

		var t = Instantiate(obtaclePrefab) as Transform;

		t.SetParent(background,false);

		t.position = new Vector2(1.05f * Util.GetCamWidth() / 2f,0);

		t.localScale = Vector2.one * 0.25f;

		var o = t.GetComponent<Obstacle>();

		o.DOColor(colors);
		o.DOMove(2, SpawnObstacle);
	}
	/// <summary>
	/// Method call when the player earns 1 point.
	/// </summary>
	public void Add1Point()
	{
        if(!isGameOver)
		    point++;
	}
	/// <summary>
	/// Method call when the player failed (collision).
	/// We will send the points to leaderboard using Very Simple Leaderboard : http://u3d.as/qxf
	/// And we will potentially show interstitial using Very Simple Ads : http://u3d.as/oWD
	/// </summary>
	public void GameOver()
	{
		GameOver(false);
	}

	/// <summary>
	/// Method call when the player failed (collision).
	/// We will send the points to leaderboard using Very Simple Leaderboard : http://u3d.as/qxf
	/// And we will potentially show interstitial using Very Simple Ads : http://u3d.as/oWD
	/// </summary>
	public void GameOver(bool fromUpOrDown)
	{
		if(isGameOver)
			return;

        if (shield.activeSelf == true && !fromUpOrDown)
            PlaySoundBreak();
        shield.SetActive(false);

        if (unbeatable && !fromUpOrDown)
        {
            Debug.Log("無敵");
            particle.Emit(30);
            Unbeatable = false;
            return;
        }

		Util.SetLastScore(point);

		isGameOver = true;

		PlaySoundFail();

		player.DisableTouch();

		DOVirtual.DelayedCall(0.3f, () => {
			Destroy(player.GetComponent<Collider2D>());
			player.DesactivateRenderer();
		});

		if(fromUpOrDown)
		{
			player.DOParticle(5);
			DOVirtual.DelayedCall(2,Util.ReloadLevel);
		}
		else
		{
			var cs = FindObjectsOfType<Collider2D>();

			foreach(var c in cs)
			{
				c.isTrigger = false;
			}
		}

        //スクショ撮影
        ButtonShare bs = FindObjectOfType<ButtonShare>();
        bs.Capture();

        StartCoroutine(_GameOver());
	}
	/// <summary>
	/// Spawn multiple particle at game over and wait a little bit (here 2 seconds) before to restart the level.
	/// </summary>
	IEnumerator _GameOver()
	{
        float time = Time.realtimeSinceStartup;

		while(true)
		{
			player.DOParticle(5);

			yield return 0;
			yield return 0;
			yield return 0;
			yield return 0;
			yield return 0;

			if(Time.realtimeSinceStartup - time > 2)
				break;
		}

        if (Random.value < 0.33f)
        {
#if UNITY_IOS || UNITY_ANDROID
            Debug.Log("インタースティシャル広告表示");
            NendAdInterstitial.Instance.Show();
            GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "ViewInterstitial", 1);
#endif
        }
        Util.ReloadLevel();
	}

}
