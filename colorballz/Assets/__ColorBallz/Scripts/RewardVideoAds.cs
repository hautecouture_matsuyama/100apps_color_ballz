﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NendUnityPlugin.AD.Video;

using NendUnityPlugin.AD;
using NendUnityPlugin.Platform;
using NendUnityPlugin.Common;
using NendUnityPlugin.Layout;

public class RewardVideoAds : MonoBehaviour {

    NendAdRewardedVideo m_RewardedVideoAd;

    //[SerializeField]
    //NendUtils.Account account;

    [SerializeField]
    private Button AdWatch;

    [SerializeField]
    private GameObject Panel;

    // Use this for initialization
    void Start () {

        AdWatch.onClick.AddListener(WatchAds);

#if UNITY_EDITOR
        AdWatch.gameObject.SetActive(false);
#endif

#if UNITY_IOS || UNITY_ANDROID

#if UNITY_IOS
        m_RewardedVideoAd = NendAdRewardedVideo.NewVideoAd("959153", "ea8888f3d024847a94221c5c5025b5ccdc8c479e");
#elif UNITY_ANDROID
        m_RewardedVideoAd = NendAdRewardedVideo.NewVideoAd("959155", "18989bc4960c806252294807d7aee87f4ac43fc0");
#endif

        m_RewardedVideoAd.AdLoaded += (instance) => {
            // 広告ロード成功のコールバック
            if (m_RewardedVideoAd.IsLoaded())
            {
                Debug.Log("リワード広告取得成功");
                AdWatch.gameObject.SetActive(true);
            }
            else
                Debug.Log("リワードで何らかの問題");
        };
        m_RewardedVideoAd.AdFailedToLoad += (instance, errorCode) => {
            // 広告ロード失敗のコールバック
            Debug.Log("リワード広告取得失敗");
            AdWatch.gameObject.SetActive(false);
        };
        m_RewardedVideoAd.AdFailedToPlay += (instance) => {
            // 再生失敗のコールバック
            Debug.Log("リワード動画再生失敗");
        };
        m_RewardedVideoAd.AdShown += (instance) => {
            // 広告表示のコールバック
        };
        m_RewardedVideoAd.AdStarted += (instance) => {
            // 再生開始のコールバック
            Debug.Log("リワード動画再生");
        };
        m_RewardedVideoAd.AdStopped += (instance) => {
            // 再生中断のコールバック
            Debug.Log("リワード広告再生中断");
        };
        m_RewardedVideoAd.AdCompleted += (instance) => {
            // 再生完了のコールバック
            Debug.Log("リワード広告再生完了");
        };
        m_RewardedVideoAd.AdClicked += (instance) => {
            // 広告クリックのコールバック
        };
        m_RewardedVideoAd.InformationClicked += (instance) => {
            // オプトアウトクリックのコールバック
        };
        m_RewardedVideoAd.AdClosed += (instance) => {
            // 広告クローズのコールバック
        };
        m_RewardedVideoAd.Rewarded += (instance, rewardedItem) => {
            // リワード報酬のコールバック

            Debug.Log("リワード反映");

            AdWatch.gameObject.SetActive(false);
            //リワード反映したことを知らせる
            Panel.SetActive(true);
            GameManager game = FindObjectOfType<GameManager>();
            game.Unbeatable = true;
        };

        m_RewardedVideoAd.Load();

#endif
    }

    void OnDestroy()
    {
        m_RewardedVideoAd.Release();
    }

    void WatchAds()
    {
        if (m_RewardedVideoAd.IsLoaded())
        {
            Debug.Log("リワード広告読み込み済み");
            m_RewardedVideoAd.Show();
        }
        else
            Debug.Log("リワード広告読み込みがまだです");

        AdWatch.gameObject.SetActive(false);
    }
}
