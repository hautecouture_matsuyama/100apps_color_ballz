﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PrefsManager{

	public static void SetUserId(string userId)
    {
        PlayerPrefs.SetString("userid", userId);
    }

    public static string GetUserId()
    {
        return PlayerPrefs.GetString("userid");
    }

    public static void SetUserName(string userName)
    {
        PlayerPrefs.SetString("username", userName);
    }

    public static string GetUserName()
    {
        return PlayerPrefs.GetString("username");
    }

    public static void SetWeeklyBestScore(string bestScore)
    {
        PlayerPrefs.SetString("bestscore", bestScore);
    }

    public static int GetWeeklyBestScore()
    {
        return PlayerPrefs.GetInt("bestscore");
    }

    public static void SetNowTime(string nowTime)
    {
        PlayerPrefs.SetString("nowTime", nowTime);
    }

    public static string GetNowTime()
    {
        return PlayerPrefs.GetString("nowTime");
    }

    public static void SetInitializedDay(string initializedDay)
    {
        PlayerPrefs.SetString("InitDay", initializedDay);
    }

    public static string GetInitializedDay()
    {
        return PlayerPrefs.GetString("InitDay");
    }
}
