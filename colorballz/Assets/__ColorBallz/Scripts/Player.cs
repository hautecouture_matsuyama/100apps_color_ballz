﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

/// <summary>
/// Attached to the Player GameObject in hierarchy.
/// In charge to change the color of the sprite.
/// In charge to spawn particles.
/// </summary>
public class Player : MonoBehaviour
{
	/// <summary>
	/// Reference to the GameManager;
	/// </summary>
	GameManager gameManager;
	/// <summary>
	/// Reference to the SpriteRenderer;
	/// </summary>
	SpriteRenderer sr;
	/// <summary>
	/// Reference to the ParticleSystem we will emit each jumps or at game over;
	/// </summary>
	public ParticleSystem particle;
	/// <summary>
	/// Reference to the TextMesh we will show each time the player earns a point;
	/// </summary>
	public TextMesh textMesh;
	/// <summary>
	/// Reference to the Collider2D;
	/// </summary>
	[NonSerialized] public Collider2D _collider;
	/// <summary>
	/// True if the player is started (we put it in true at first touch).
	/// </summary>
	bool isStarted = false;
	/// <summary>
	/// Some initialization.
	/// </summary>
	void Awake()
	{
		gameManager = FindObjectOfType<GameManager>();
		sr = GetComponent<SpriteRenderer>();
		_collider = GetComponent<Collider2D>();
		_collider.enabled = false;
		Color c = textMesh.color;
		c.a = 0;
		textMesh.color = c;
	}
	/// <summary>
	/// Get the color of the ball.
	/// </summary>
	public Color GetColor()
	{
		return sr.color;
	}
	/// <summary>
	/// Desactivate the circle sprite of the ball.
	/// </summary>
	public void DesactivateRenderer()
	{
		sr.enabled = false;
	}
	/// <summary>
	/// Method called when we are ready to play, and enabled the InputTouch.OnTouchScreen is enabled.
	/// </summary>
	public void DOStart()
	{
		if(isStarted)
			return;
		
		_collider.enabled = true;
		InputTouch.OnTouchScreen += OnTouchScreen;
		isStarted = true;
	}
	/// <summary>
	/// Disable the touch detection.
	/// </summary>
	public void DisableTouch()
	{
		InputTouch.OnTouchScreen -= OnTouchScreen;
	}
	/// <summary>
	/// Method called each time the player touch the screen, when InputTouch.OnTouchScreen is enabled.
	/// </summary>
	void OnTouchScreen()
	{
		GetComponent<Rigidbody2D>().velocity = Vector2.up * 5;
		gameManager.PlaySoundJump();
		DOParticle(5);
	}
	/// <summary>
	/// Anim the textMesh when the player earns a point.
	/// </summary>
	public void DOPointAnim()
	{
		Color c = sr.color;

		c.a = 0;

		textMesh.color = c;

		textMesh.transform.DOLocalMoveY(+0.15f, 1f);

		DOVirtual.Float(0, 1, 0.3f, (float f) => {
			c.a = f;
			textMesh.color = c;
		});

		DOVirtual.Float(1, 0, 0.3f, (float f) => {
			c.a = f;
			textMesh.color = c;
		}).SetDelay(0.4f);
	}
	/// <summary>
	/// Change the color of the player = the ball = his sprite renderer = the circle.
	/// </summary>
	public void DOColor(List<Color> colors)
	{
		colors.Shuffle();
		int i = UnityEngine.Random.Range(0, colors.Count);
		Color c = colors[i];

		if(c.IsEqual(sr.color))
		{
			DOColor(colors);
		}
		else
		{
			sr.color = c;
		}
	}
	/// <summary>
	/// Spawn the particle.
	/// </summary>
	public void DOParticle(int count)
	{

		List<Color> list = new List<Color>();

		foreach(Color color in gameManager.colors)
		{
			list.Add(color);
		}

		list.Shuffle();

		int rand = UnityEngine.Random.Range(0, list.Count);
		Color c = list[rand];
		list.RemoveAt(rand);

        var main = particle.main;
        main.startColor = c;

        particle.Emit(count);

		DOVirtual.DelayedCall(0.05f,() => {
			rand = UnityEngine.Random.Range(0, list.Count);
			c = list[rand];
			list.RemoveAt(rand);
            main.startColor = c;
            particle.Emit(count);
		});
	}
}