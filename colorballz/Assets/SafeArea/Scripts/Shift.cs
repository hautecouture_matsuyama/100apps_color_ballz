﻿
//  2019/7/30   作成：兵頭
//  * Unity 2017.x以降であればこれらのコードは不要。
//  iPhoneXシリーズとiPadProシリーズと使い分けたい場合は別途コードを追加する必要あり。


//  特に指定がない場合はセーフエリア分をピクセル単位でずらすので[Canvas Scaler]の[UI Scale Mode]が[Constant Pixel Size]に設定されている必要がある。
//  他の設定の場合、端末毎にずれに誤差が生まれるので別の方法でずらす。ずらす基準はXRのポイント(414x896)とする。
//  その場合、X,XS(375x812)はXRと比べて小さいので余分にずれることになる。X/XR=1.xyz...

//2019/7/16時点でセーフエリアが存在する端末はiPadPro11、iPadPro12.9(3世代)、iPhoneX,XS、iPhoneXR、iPhoneXSMaxの5つ

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.iOS;

public class Shift : MonoBehaviour {

    public enum direction
    {
        Top = 0,
        Bottom = 1,
        Left = 2,
        Right = 3
    };
    [Header("---Position---")]
    [SerializeField]
    private bool defaultShift;
    [SerializeField]
    private direction dir;

    [SerializeField]
    private Vector2 ChangeValue;
    [SerializeField]
    private bool isChangePosition;

    [Header("---Scale---")]
    [SerializeField]
    private bool isChangeScale;
    [SerializeField]
    private bool isPixelScale;

    private int standardX = 414;
    private int standardY = 896;


    // Use this for initialization
    void Start ()
    {
        RectTransform rect = GetComponent<RectTransform>();
        var devices = SafeAreaManager.Instance.GetDevices();
        Debug.Log("このデバイス名：" + devices);

#if UNITY_EDITOR
#if UNITY_IOS
        Debug.Log("iOS");
        if (isChangeScale && !isPixelScale)
        {
            float rate = AspectManager.Instance.GetAspectRate();
            //縮小だけに対応させる。
            if (rate < 1)
                rect.localScale = new Vector3(rate, rate, rate);
        }
        else if (isChangeScale && isPixelScale)
        {

            float rate = AspectManager.Instance.rate;

            rect.sizeDelta = new Vector2(rect.sizeDelta.x * rate, rect.sizeDelta.y * rate);
            rect.anchoredPosition = new Vector2(0, -rect.sizeDelta.y / 2);
        }

        var version = Device.generation;
        Debug.Log(version);
        if (isChangePosition && (version == DeviceGeneration.iPhoneX || version == DeviceGeneration.iPhoneUnknown))
        {
            Debug.Log("iPhoneXシリーズ");
            if (defaultShift)
            {
                Debug.Log(gameObject.name + ":初期値移動");
                rect.anchoredPosition += SafeAreaManager.Instance.GetShift(dir);
            }
            else
            {
                Debug.Log(gameObject.name + ":指定値移動");
                rect.anchoredPosition += SetShift(ChangeValue);
            }
        }
        //iPadの場合、世代で分けられてるものがあるので注意(12.9インチは第3世代だけセーフエリア対象)
        else if ((devices.ToString().Contains("iPadPro") && version == DeviceGeneration.iPadUnknown && AspectManager.Instance.GetAspectRate(false) < 0.75f))
        {
            Debug.Log("iPadPro");
            if (defaultShift)
            {
                Debug.Log(gameObject.name + ":初期値移動");
                rect.anchoredPosition += SafeAreaManager.Instance.GetShift(dir);
            }
            else
            {
                Debug.Log(gameObject.name + ":指定値移動");
                rect.anchoredPosition += SetShift(ChangeValue);
            }
        }
        else
        {
            Debug.Log("その他");
        }

#else
        Debug.Log("iOS以外");
#endif
#else
#if UNITY_IOS
        //アスペクト比に合わせて縮小
        if (isChangeScale && !isPixelScale)
        {
            float rate = AspectManager.Instance.GetAspectRate();
            //縮小だけに対応させる。
            if (rate < 1)
                rect.localScale = new Vector3(rate, rate, rate);
        }
        else if (isChangeScale && isPixelScale)
        {

            float rate = AspectManager.Instance.rate;

            rect.sizeDelta = new Vector2(rect.sizeDelta.x * rate, rect.sizeDelta.y * rate);
            rect.anchoredPosition = new Vector2(0, -rect.sizeDelta.y / 2);
        }

        //エディタ上では端末情報を選択できないのでアスペ比で判別する。
        if (isChangePosition)
        {
            if (AspectManager.Instance.GetAspectRate(false) < 0.75f && AspectManager.Instance.GetAspectRate(false) > 0.6f)
            {
                Debug.Log("iPadProシリーズ");
                if (defaultShift)
                {
                    Debug.Log(gameObject.name + ":初期値移動");
                    rect.anchoredPosition += SafeAreaManager.Instance.GetShift(dir);
                }
                else
                {
                    Debug.Log(gameObject.name + ":指定値移動");
                    rect.anchoredPosition += SetShift(ChangeValue);
                }
            }
            else if (AspectManager.Instance.GetAspectRate(false) < 0.5f)
            {
                Debug.Log("iPhoneXシリーズ");
                if (defaultShift)
                {
                    Debug.Log(gameObject.name + ":初期値移動");
                    rect.anchoredPosition += SafeAreaManager.Instance.GetShift(dir);
                }
                else
                {
                    Debug.Log(gameObject.name + ":指定値移動");
                    rect.anchoredPosition += SetShift(ChangeValue);
                }
            }
        }
#endif
#endif
    }

    private Vector2 SetShift(Vector2 v)
    {
        Vector2 shift = v;
        float rate = 1;
        int size;

        if (v.x == 0 && v.y !=　0)
        {
            size = SafeAreaManager.Instance.pointX;
            Debug.Log(size);
            rate = (float)standardX / size;
        }
        else if (v.x != 0 && v.y == 0)
        {
            size = SafeAreaManager.Instance.pointY;
            Debug.Log(size);
            rate = (float)standardY / size;
        }
        Debug.Log("基準と比べた比率：" + rate);
        shift = shift * rate;

        Debug.Log("ずらす量" + shift);
        return shift;
    }
}
