﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DebugScreenShot : MonoBehaviour {

    private string fileName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.S)){
            fileName = "image";
            Capture();
        }
#endif
    }

    void Capture()
    {
        Debug.Log("画面キャプチャ");
        Application.CaptureScreenshot("Assets/__ColorBallz/Images/image.png");
    }

   
}
