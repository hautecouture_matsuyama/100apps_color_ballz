﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Attached to each element (4 in total) of the obstacle = each child = each SpriteRenderer of each Obstacle.
/// </summary>
public class Brick : MonoBehaviour 
{
	/// <summary>
	/// Reference to the parent Obstacle.
	/// </summary>
	Obstacle obstacle;
	/// <summary>
	/// Reference to the SpriteRenderer composing each brick.
	/// </summary>
	SpriteRenderer sr;
	/// <summary>
	/// Initialization.
	/// </summary>
	void Awake()
	{
		obstacle = GetComponentInParent<Obstacle>();
		sr = GetComponent<SpriteRenderer>();
	}
	/// <summary>
	/// Listen collision for vertical bricks.
	/// </summary>
	void OnTriggerEnter2D(Collider2D other)
	{
		var player = other.GetComponent<Player>();
		if(player != null)
		{
			obstacle.OnTestTrigger2D(player, sr.color);
		}
	}
	/// <summary>
	/// Listen collision for horizontal bricks.
	/// </summary>
	void OnCollisionEnter2D(Collision2D coll) 
	{
		FindObjectOfType<GameManager>().GameOver(true);
	}
}
