﻿
//  2019/7/31   作成：兵頭
//  FirebaseのSDKは別途入手する必要あり。詳しくは　https://firebase.google.com/docs/unity/setup?hl=ja
//  アイコンを使う場合はコメントアウトしている箇所を解除すると使える。「icon」で検索するとすぐ出る。

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;

public class RankingManager : SingletonMonoBehaviour<RankingManager>
{

    //FirebaseのデータベースURL
    [SerializeField]
    private string _databaseUrl;

    [SerializeField]
    private string _rankingChildItem;

    [SerializeField]
    private GameObject _content;
    [SerializeField]
    private GameObject _createNameCanvas;

    //public Text MyRankText;

    [SerializeField]
    private int rankingCount;


    //ランキングのキャンバス.
    public GameObject RankingCanvas;
    [SerializeField]
    private Button _closeButton;
    [SerializeField]
    private Button _showRankingButton;

    [SerializeField]
    private InputField _inputfield;

    [SerializeField]
    private Text _myrankText;

    //[SerializeField]
    //private Texture[] textures;

    private DatabaseReference _rankingDb;

    //ランキングのランク用のリスト（例：1位←ここ）
    //ランキングの桁数が変わった時に対応するためのリスト
    //五桁まで対応できる位置
    private List<GameObject> _rankList = new List<GameObject>();

    //ランキングの名前用のリスト（例：たけし←ここ）
    private List<GameObject> _nameList = new List<GameObject>();

    //ランキングのスコア用のリスト
    //五桁までのスコアに対応
    private List<GameObject> scoreList = new List<GameObject>();

    //使用アイコン
    //private List<GameObject> iconList = new List<GameObject>();

    //ユーザーのアカウントID（自分のIDを検索するときに使用）
    private List<GameObject> IDList = new List<GameObject>();

    //後で消す
    private System.Random random = new System.Random();

    private void Awake()
    {
        if (this != Instance)
        {
            Destroy(this);
            return;
        }
    }

    public void Start()
    {
        Initialize();

        //scoreList[0].transform.parent.GetComponent<Image>().color = new Color(0.263f, 0.941f, 0.324f, 225);
        RankingCanvas.SetActive(false);

    }

    //アカウント判定
    //既にユーザーIDがあれば、"true"が返ってくる
    public bool SignIn()
    {
        if (!PlayerPrefs.HasKey("userid") || !PlayerPrefs.HasKey("username"))
            return false;
        else
            return true;
    }

    public void Initialize()
    {

        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl(_databaseUrl);
        _rankingDb = FirebaseDatabase.DefaultInstance.GetReference(_rankingChildItem);

        //ランキングのノードをすべて取得
        foreach (var n in _content.gameObject.GetChildren())
        {
            //各リストに初期値挿入
            switch (n.name)
            {
                case "rText":
                    _rankList.Add(n);
                    break;
                case "nText":
                    _nameList.Add(n);
                    break;
                case "sText":
                    scoreList.Add(n);
                    break;
                case "uidText":
                    IDList.Add(n);
                    break;
                //case "icon":
                //    iconList.Add(n);
                //    break;
            }
        }
    
        //初期値代入
        for (int i = 0; i < _rankList.Count; i++)
        {
            string r = "th";
            switch (i + 1)
            {
                case 1:r = "st";break;
                case 2:r = "nd";break;
                case 3:r = "rd";break;
                default:r = "th";break;
            }

            _rankList[i].GetComponent<Text>().text = i + 1 + r;
            _nameList[i].GetComponent<Text>().text = "No Data";
            scoreList[i].GetComponent<Text>().text = "-----";
            //iconList[i].GetComponent<RawImage>().texture = null;
            //iconList[i].GetComponent<RawImage>().color = new Color(1, 1, 1, 0);
        }

        //既に登録しているか？
        if (!SignIn())
        {
            _createNameCanvas.SetActive(true);
            _inputfield.text = SetDefaultName();
        }

        GetRankingData();

        Debug.Log("ユーザー名：" + PrefsManager.GetUserName());
        Debug.Log("ユーザーID：" + PrefsManager.GetUserId());

        //不正な名前があるならもう一度名前を入力させる。ランキングのスコアは残す。

        //注意！！
        //もしuseridが存在しない時に読んでしまうと親のノード、ランキング全体が削除されるので必ずidがあるか確認すること

        if (PrefsManager.GetUserName().Length > _inputfield.characterLimit && PlayerPrefs.HasKey("userid"))
        {
            Debug.Log("名前の文字数が多いようです");
            DatabaseReference deleteNode = FirebaseDatabase.DefaultInstance.GetReference("Ranking/" + PrefsManager.GetUserId());
            deleteNode.RemoveValueAsync();
            PlayerPrefs.DeleteKey("userid");
            PlayerPrefs.DeleteKey("username");
            _createNameCanvas.SetActive(true);
            _inputfield.text = SetDefaultName();
        }

        _showRankingButton.onClick.AddListener(ShowRanking);
        _closeButton.onClick.AddListener(CloseButton);
    }

    //ランキング送信処理
    public void SendRanking(int score)
    {
        //GetMyRankingDate();
        string userid = PrefsManager.GetUserId();
        string _name = PrefsManager.GetUserName();

        var id = userid.Substring(0, 8);
        PrefsManager.SetUserId(id);
        userid = PrefsManager.GetUserId();

        Dictionary<string, object> itemMap = new Dictionary<string, object>();

        itemMap.Add("name", _name);
        itemMap.Add("score", score);
        //Debug.Log(CharacterManager.Instance.CurrentCharacterIndex);
        //itemMap.Add("icon", CharacterManager.Instance.CurrentCharacterIndex);

        Dictionary<string, object> map = new Dictionary<string, object>();
        map.Add(userid, itemMap);

        _rankingDb.UpdateChildrenAsync(map);

    }

    //アカウント（名前）が登録済みがチェック
    //Awakeで呼び出すのが望ましい？
    public bool CheckAccount()
    {
        //登録済みか？
        return false;
    }

    //ユーザー名登録処理
    //ローカルに名前とID保存しといて、以降アプリ起動した時にローカル常にデータあれば登録済み。ないなら名前入力画面へ。
    public void SignUp(string username)
    {
        //リネームの場合の処理
        if (PlayerPrefs.HasKey("userid"))
            PlayerPrefs.DeleteKey("username");
        else
        {
            Guid guid = Guid.NewGuid();
            string userid = guid.ToString();
            userid = userid.Substring(0, 8);
            PrefsManager.SetUserId(userid);
        }

        PrefsManager.SetUserName(username);

        Debug.Log("ユーザーID：" + PrefsManager.GetUserId());
        Debug.Log("ユーザー名：" + PrefsManager.GetUserName());

        //既にハイスコアがある(ランキング実装前にプレイしていた)ユーザーはランキングに登録する。
        if (PlayerPrefs.HasKey("BEST_SCORE"))
        {
            Debug.Log("スコア情報が見つかりました。");
            int score = Util.GetBestScore();
            Debug.Log("ハイスコア：" + score);
            SendRanking(score);
            GetRankingData();
        }

        _createNameCanvas.gameObject.SetActive(false);
    }

    public void ShowRanking()
    {
        RankingCanvas.SetActive(true);
    }

    public void GetRankingData()
    {
        List<string> _scoreList = new List<string>();
        List<string> nameList = new List<string>();
        List<string> _idList = new List<string>();

        //List<string> _iconList_str = new List<string>();

        _rankingDb.OrderByChild("score").LimitToLast(rankingCount).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
                Debug.Log("ランキング取得失敗");

            else if (task.IsCompleted)
            {
                //int[] _iconList = new int[0];

                DataSnapshot snapshot = task.Result;
                IEnumerator<DataSnapshot> en = snapshot.Children.GetEnumerator();

                List<DataSnapshot> list = new List<DataSnapshot>();
                list.Add(snapshot);

                while (en.MoveNext())
                {
                    DataSnapshot data = en.Current;
                    nameList.Add((string)data.Child("name").GetValue(true));
                    _scoreList.Add(data.Child("score").GetValue(true).ToString());
                    _idList.Add(data.Key);
                    //_iconList_str.Add(data.Child("icon").GetValue(true).ToString());
                }

                //降順で並び替え
                _scoreList.Reverse();
                nameList.Reverse();
                _idList.Reverse();
                //_iconList_str.Reverse();

                for (int i = 0; i < 100; i++)
                {
                    if (i + 1 > _scoreList.Count)
                    {
                        _scoreList.Add("0");
                    }

                    if (int.Parse(_scoreList[i]) == 0)
                    {
                        scoreList[i].GetComponent<Text>().text = "-----";
                    }
                    else
                    {
                        //Array.Resize(ref _iconList, _iconList.Length + 1);
                        //_iconList[i] = int.Parse(_iconList_str[i]);

                        scoreList[i].GetComponent<Text>().text = _scoreList[i];
                        //iconList[i].GetComponent<RawImage>().texture = textures[_iconList[i]];
                        //iconList[i].GetComponent<RawImage>().color = new Color(1, 1, 1, 1);
                    }
                    //スコアが0なら名前は"Player"として表示させる。IDはnull
                    _nameList[i].GetComponent<Text>().text = (int.Parse(_scoreList[i]) == 0) ? "No Data" : _nameList[i].GetComponent<Text>().text = nameList[i];
                    IDList[i].GetComponent<Text>().text = (int.Parse(_scoreList[i]) == 0) ? null : _idList[i];

                    if (i > 0)
                    {
                        if (int.Parse(_scoreList[i]) != 0 && int.Parse(_scoreList[i]) == int.Parse(_scoreList[i - 1]))
                        {
                            _rankList[i].GetComponent<Text>().text = _rankList[i - 1].GetComponent<Text>().text;
                        }
                        else
                        {
                            int rank = i + 1;
                            string r = "th";
                            switch (rank)
                            {
                                case 1: r = "st"; break;
                                case 2: r = "nd"; break;
                                case 3: r = "rd"; break;
                                default: r = "th"; break;
                            }
                            _rankList[i].GetComponent<Text>().text = rank + r;
                        }
                    }
                }
                _myrankText.text = GetMyRankingData();
            }
        });
    }

    public string GetMyRankingData()
    {
        if (PlayerPrefs.GetInt("BEST_SCORE", 0) == 0)
            return "No Score";

        string myUserID = PrefsManager.GetUserId();
        int scoreCnt = 0;

        while (scoreCnt < 100)
        {
            if (IDList[scoreCnt].GetComponent<Text>().text == myUserID)
            {
                scoreList[scoreCnt].transform.parent.GetComponent<Image>().color = new Color(0f, 0.686f, 0f, 225);
                string str = Regex.Replace(_rankList[scoreCnt].GetComponent<Text>().text, @"[^0-9]", "");
                string str2 = str;
                return str2;
            }
            else
                scoreList[scoreCnt].transform.parent.GetComponent<Image>().color = new Color(0, 0, 0, 0);

            scoreCnt++;
        }
        return "100 or less";
    }

    public void Rename()
    {
        Debug.Log("リネーム");
        _createNameCanvas.SetActive(true);
        _inputfield.text = PrefsManager.GetUserName();
    }

    public string SetDefaultName()
    {
        string defaultName;
        SystemLanguage lang = Application.systemLanguage;
        TextAsset nameTextFile;

        if(lang == SystemLanguage.Japanese)
            nameTextFile = Resources.Load("Text/defaultname_Ja", typeof(TextAsset)) as TextAsset;
        else if(lang == SystemLanguage.Chinese || lang == SystemLanguage.ChineseSimplified || lang == SystemLanguage.ChineseTraditional)
            nameTextFile = Resources.Load("Text/defaultname_Ch", typeof(TextAsset)) as TextAsset;
        else
            nameTextFile = Resources.Load("Text/defaultname_En", typeof(TextAsset)) as TextAsset;

        string[] nameList = nameTextFile.text.Split("\n"[0]);
        int idx = random.Next(nameList.Length);
        defaultName = nameList[idx].Replace("\r", "");

        //念のためNGワードチェックを入れる
        string after = Ngword.ConvertNgWordPartially(defaultName);
        if(after != defaultName)
        {
            return SetDefaultName();
        }

        return defaultName;
    }

    private void CloseButton()
    {
        RankingCanvas.SetActive(false);
    }


}
