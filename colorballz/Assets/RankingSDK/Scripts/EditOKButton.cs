﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;


public class EditOKButton : MonoBehaviour {

    [SerializeField]
    private InputField _inputField;

    [SerializeField]
    private Text ErrorText;
    
    [SerializeField]
    private GameObject CreateNameCanvas;

    [SerializeField]
    private bool isHit = false;

    private SystemLanguage language;

    public void HitNGWord()
    {
        isHit = true;
    }

    private void Awake()
    {
        language = Application.systemLanguage;
    }

    public void CreateAccount()
    {

        string strAfter = "";

        //入力されているか？
        if (_inputField.text == string.Empty)
            return;

        //日本語・中国語なら5文字、それ以外なら7文字まで入力されている内容で登録する
        if (language == SystemLanguage.Japanese || language == SystemLanguage.Chinese
            || language == SystemLanguage.ChineseSimplified || language == SystemLanguage.ChineseTraditional)
        {
            if (_inputField.text.Length > 5) strAfter = _inputField.text.Remove(5);
        }
        else  
        {
            if (_inputField.text.Length > 7) strAfter = _inputField.text.Remove(7);
        }

        strAfter = _inputField.text;

        //NGワードが含まれていないか確認
        _inputField.text = Ngword.ConvertNgWordPartially(strAfter);

        if (isHit)
        {
            isHit = false;
            ErrorMessage();
            return;
        }

        _inputField.text = "*****";

        ErrorText.text = "";

        //ユーザー名登録
        RankingManager.Instance.SignUp(strAfter);

        CreateNameCanvas.SetActive(false);
    }

    void ErrorMessage()
    {
        if (language == SystemLanguage.Japanese)
            ErrorText.text = "* 不適切な表現が含まれています！";
        else if (language == SystemLanguage.Chinese || language == SystemLanguage.ChineseSimplified || language == SystemLanguage.ChineseTraditional)
            ErrorText.text = "* 输入名称含有非法字符!";
        else
            ErrorText.text = "* Contains inappropriate words!";
    }

}
