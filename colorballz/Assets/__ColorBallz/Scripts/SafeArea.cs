﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.iOS;

public class SafeArea : MonoBehaviour {

    [SerializeField]
    private Vector3 ChangeValue;

	// Use this for initialization
	void Start () {
#if UNITY_IPHONE
        var version = Device.generation;
        if (version == DeviceGeneration.iPhoneX || version == DeviceGeneration.iPhoneUnknown)
        {
            Debug.Log("iPhoneX以降の端末");
            RectTransform rect = this.gameObject.GetComponent<RectTransform>();
            rect.localPosition += ChangeValue;
        }
#endif
    }

    // Update is called once per frame
    void Update () {
		
	}
}
