﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

/// <summary>
/// Attached to the prefab Obstacle. In Charge of his move. Represent the Obstacles up and down (ie. horizontal) and the obstacles who move from right to left (vertical)
/// </summary>
public class Obstacle : MonoBehaviour 
{
	/// <summary>
	/// Reference to the GameManager.
	/// </summary>
	GameManager gameManager;
	/// <summary>
	/// Reference to each part (4 in total) of the wall (ie. obstacles). Each part is a SpriteRenderer.
	/// </summary>
	public List <SpriteRenderer> srs;
	/// <summary>
	/// The obstacle is a up obstacle? (on the top of the game view).
	/// </summary>
	public bool isUp = false;
	/// <summary>
	/// The obstacle is a down obstacle? (on the bottom of the game view).
	/// </summary>
	public bool isDown = false;
	/// <summary>
	/// Return true if it's a Up obstacle or a Down obstacle.
	/// </summary>
	bool isUpOrDown
	{
		get
		{
			return isUp || isDown;
		}
	}
	/// <summary>
	/// Initialization.
	/// </summary>
	void Awake()
	{
		gameManager = FindObjectOfType<GameManager>();
		for(int i = 0; i < srs.Count; i++)
		{
			srs[i].color = gameManager.colors[i];
		}
	}
	/// <summary>
	/// Positioning the obstacle.
	/// </summary>
	void Start()
	{
		if(isUpOrDown)
		{
			var b = FindObjectOfType<Background>();
			var sr = b.GetComponent<SpriteRenderer>();

			float myHeigh = srs[0].bounds.size.y;

			if(isUp)
				transform.position = new Vector2(transform.position.x, + sr.bounds.size.y * 0.5f + myHeigh * 0.5f);
			else
				transform.position = new Vector2(transform.position.x, - sr.bounds.size.y * 0.5f - myHeigh * 0.5f);
			
			DOUpOrDown();
		}
	}
	/// <summary>
	/// Position Up and Down obstacles.
	/// And do the move (parallax).
	/// </summary>
	void DOUpOrDown()
	{
		if(isUp)
		{
			transform.DOLocalMoveX(transform.localPosition.x - 1, 1).SetEase(Ease.Linear).OnComplete(() => {
				if(transform.localPosition.x < 0.1 && transform.localPosition.x > -0.1)
				{
					transform.localPosition = new Vector2(0, transform.localPosition.y);
					transform.DOLocalMoveX(transform.localPosition.x - 1, 1).SetEase(Ease.Linear).OnComplete(() => {
						transform.localPosition = new Vector2(+1, transform.localPosition.y);
						DOUpOrDown();
					});
				}
				else
				{
					transform.localPosition = new Vector2(+1, transform.localPosition.y);
					DOUpOrDown();
				}
			});	
		}
		else
		{
			transform.DOLocalMoveX(transform.localPosition.x + 1, 1).SetEase(Ease.Linear).OnComplete(() => {
				if(transform.localPosition.x < 0.1 && transform.localPosition.x > -0.1)
				{
					transform.localPosition = new Vector2(0, transform.localPosition.y);
					transform.DOLocalMoveX(transform.localPosition.x + 1, 1).SetEase(Ease.Linear).OnComplete(() => {
						transform.localPosition = new Vector2(-1, transform.localPosition.y);
						DOUpOrDown();
					});
				}
				else
				{
					transform.localPosition = new Vector2(-1, transform.localPosition.y);
					DOUpOrDown();
				}
			});	
		}
	}
	/// <summary>
	/// Change the color of the obstacle = change the color of the 4 sprites.
	/// </summary>
	public void DOColor(List<Color> colors)
	{
		srs.Shuffle();

		for(int i = 0; i < colors.Count; i++)
		{
			srs[i].color = colors[i];
		}
	}
	/// <summary>
	/// Do the obstacle move from right to left for vertical obstacle.
	/// </summary>
	public void DOMove(float time, Action callback)
	{
		transform.DOMoveX(-1.05f * Util.GetCamWidth() / 2f, time).SetEase(Ease.Linear).OnComplete(() => {
			if(callback != null)
				callback();

			if(!isUpOrDown)
				Destroy(gameObject);
		});
	}
	/// <summary>
	/// Do the obstacle move from right to left for vertical obstacle.
	/// </summary>
	public void OnTestTrigger2D(Player player, Color color)
	{
		if(isUpOrDown)
		{
			gameManager.GameOver(true);
			return;
		}

		bool isEquals = player.GetColor().IsEqual(color);

		if(isEquals)
		{
			var cs = transform.GetComponentsInChildren<Collider2D>();
			foreach(var c in cs)
			{
				c.enabled = false;
			}
			gameManager.Add1Point();
		}
		else
		{
			gameManager.GameOver();
		}
	}
}
