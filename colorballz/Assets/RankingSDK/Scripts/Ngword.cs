﻿using UnityEngine;
using System.Text;

public static class Ngword
{
    public static string ConvertNgWordPartially(string name)
    {
        string convertName = name;
        StringBuilder asterisk = new StringBuilder();

        //ngwordリストのパスは任意のパスに変更してください
        TextAsset nameTextFile = Resources.Load("Text/ngword", typeof(TextAsset)) as TextAsset;

        string[] ngwordList = nameTextFile.text.Split("\n"[0]);

        EditOKButton ok = MonoBehaviour.FindObjectOfType<EditOKButton>();

        foreach (string ng in ngwordList)
        {
            var ngword = ng.Replace("\r", "");
            name = convertName;

            //アルファベットが含まれているかどうか
            if (IsAlphabet(ngword.ToCharArray()))
            {
                //大文字変換
                var upperName = name.ToUpper();

                //NGワードに引っかかった場合
                if (upperName.Contains(ngword))
                {
                    ok.HitNGWord();

                    int ngIndex = upperName.IndexOf(ngword);
                    int ngwordLength = ngword.Length;

                    string tmpNgString = name.Substring(ngIndex, ngwordLength);

                    for (int i = 0; i < ngwordLength; i++)
                    {
                        asterisk.Append("*");
                    }

                    convertName = name.Replace(tmpNgString, asterisk.ToString());
                }
            }
            else
            {
                var hiraganaName = name.ToHiragana();

                if (hiraganaName.Contains(ngword))
                {
                    ok.HitNGWord();

                    int ngIndex = hiraganaName.IndexOf(ngword);
                    int ngwordLength = ngword.Length;

                    string tmpNgString = name.Substring(ngIndex, ngwordLength);

                    for (int i = 0; i < ngwordLength; i++)
                    {
                        asterisk.Append("*");
                    }

                    convertName = name.Replace(tmpNgString, asterisk.ToString());
                }
            }
        }

        if (convertName.Length > 6)
            convertName = convertName.Substring(0, 6);

        Debug.Log("確認後：" + convertName);
        return convertName;
    }

    public static bool IsAlphabet(char[] c)
    {
        return (c[0] >= 'A' && c[0] <= 'z') ? true : false;
    }
}