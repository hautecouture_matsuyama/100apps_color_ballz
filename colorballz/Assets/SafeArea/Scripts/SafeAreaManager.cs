﻿
//  2019/8/1    作成：兵頭
//  こちらではエディタ上でセーフエリアを示す画像を表示させる。画像はスクショを拡大したものなので若干違いが生じるので注意
//  作成時以降に新しく出てきた端末には未対応なので、その場合は追記してGitを更新すると幸せになれる。
//  端末情報は解像度を参照している。

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.iOS;

public class SafeAreaManager : MonoBehaviour {

    public enum IOSDevices
    {
        iPhoneXorXS = 0,
        iPhoneXR = 1,
        iPhoneXSMax = 2,
        iPadPro11Inch = 3,
        iPadPro12_9Inch = 4,
        Unknown = 5,
    }

    public static SafeAreaManager Instance;

    public static IOSDevices devices;

    private Vector2 defaultSize = new Vector2(37,37);
    private Vector2 defaultTopSize = new Vector2(220, 30);

    [SerializeField]
    private GameObject safeAreaCanvas;

    //セーフエリア外(黒の半透明な部分)
    [SerializeField]
    private RectTransform safeTop;
    [SerializeField]
    private RectTransform safeBottom;
    [SerializeField]
    private RectTransform safeLeft;
    [SerializeField]
    private RectTransform safeRight;

    //iPhoneのカメラの部分
    [SerializeField]
    private RectTransform cameraTop;
    [SerializeField]
    private RectTransform cameraLeft;

    //4隅の丸い部分
    [SerializeField]
    private RectTransform leftTop;
    [SerializeField]
    private RectTransform leftBottom;
    [SerializeField]
    private RectTransform rightTop;
    [SerializeField]
    private RectTransform rightBottom;

    //iPad版の4隅の画像
    [SerializeField]
    private Sprite leftTop_pad;
    [SerializeField]
    private Sprite leftBottom_pad;
    [SerializeField]
    private Sprite rightTop_pad;
    [SerializeField]
    private Sprite rightBottom_pad;

    public static Vector2 fix;      //縦横のピクセル数

    [SerializeField]
    private Safe safe;

    [SerializeField]
    private int iPhoneNum;      //どこの要素までがiPhoneか？

    public int pointX, pointY;
    private bool isVertical;
    private int retina;

    void Awake()
    {

        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        fix = new Vector2(Screen.width, Screen.height);
        isVertical = (fix.x < fix.y);

#if !UNITY_EDITOR
        JudgeDevice();
#endif

        Debug.Log("デバイスの解像度：("+Screen.width+","+Screen.height+")");

        //何の端末か判定する。

        if (isVertical)
        {
            Debug.Log("縦持ち");
            cameraLeft.gameObject.SetActive(false);
            for(int i = 0; i < safe.devices.Length; i++)
            {
                if (fix == safe.devices[i].rectVertical * safe.devices[i].retina)
                {
                    devices = (IOSDevices)i;
                    retina = safe.devices[i].retina;
                    pointX = (int)safe.devices[i].rectVertical.x;
                    pointY = (int)safe.devices[i].rectVertical.y;
                    Debug.Log("一致する解像度：" + safe.devices[i].rectVertical * safe.devices[i].retina);
                    Debug.Log("予測される機種名：" + safe.devices[i].deviceName);
                    if (i <= iPhoneNum)
                    {
                        Vector2 newSize = defaultTopSize * safe.devices[i].retina;
                        cameraTop.sizeDelta = newSize;
                        cameraTop.anchoredPosition = new Vector2(0, -cameraTop.sizeDelta.y / 2);
                        cameraTop.gameObject.SetActive(true);
                    }
                    else
                    {
                        ChangePad();
                        cameraTop.gameObject.SetActive(false);
                    }

                    SetSafeArea(safe.devices[i]);
                    SetInvisibleArea(safe.devices[i]);
                    safeAreaCanvas.SetActive(true);

#if !UNITY_EDITOR
                    safeAreaCanvas.SetActive(false);
#endif
                    return;
                }
                else
                {
                    devices = IOSDevices.Unknown;
                    safeAreaCanvas.SetActive(false);
                }
            }
            devices = IOSDevices.Unknown;
            Debug.Log("確認：デバイス情報:" + devices);
            safeAreaCanvas.SetActive(false);
        }
        //横向き
        else
        {
            Debug.Log("横持ち");
            cameraLeft.gameObject.SetActive(true);
            for (int i = 0; i < safe.devices.Length; i++)
            {
                if (fix == safe.devices[i].rectHorizontal * safe.devices[i].retina)
                {
                    devices = (IOSDevices)i;
                    retina = safe.devices[i].retina;
                    pointX = (int)safe.devices[i].rectHorizontal.x;
                    pointY = (int)safe.devices[i].rectHorizontal.y;
                    Debug.Log("一致する解像度：" + safe.devices[i].rectHorizontal * safe.devices[i].retina);
                    Debug.Log("予測される機種名：" + safe.devices[i].deviceName);
                    if (i <= iPhoneNum)
                    {
                        Vector2 newSize = defaultTopSize * safe.devices[i].retina;
                        cameraLeft.sizeDelta = newSize;
                        cameraLeft.anchoredPosition = new Vector2(cameraLeft.sizeDelta.y / 2, 0);
                        cameraLeft.gameObject.SetActive(true);
                    }
                    else
                    {
                        ChangePad();
                        cameraLeft.gameObject.SetActive(false);
                    }

                    SetSafeArea(safe.devices[i]);
                    SetInvisibleArea(safe.devices[i]);
                    safeAreaCanvas.SetActive(true);

#if !UNITY_EDITOR
                    safeAreaCanvas.SetActive(false);
#endif
                    return;
                }
                else
                {
                    devices = IOSDevices.Unknown;
                    safeAreaCanvas.SetActive(false);
                }
            }
            safeAreaCanvas.SetActive(false);
        }

#if !UNITY_EDITOR
        safeAreaCanvas.SetActive(false);
#endif
    }

    void SetSafeArea(Safe.IOSDevice s)
    {
        if (isVertical) {
            safeTop.sizeDelta = new Vector2(0, s.safeVertical.top * s.retina);
            safeBottom.sizeDelta = new Vector2(0, s.safeVertical.bottom * s.retina);
            safeLeft.sizeDelta = new Vector2(s.safeVertical.left * s.retina, 0);
            safeRight.sizeDelta = new Vector2(s.safeVertical.right * s.retina, 0);

            //サイズが0のオブジェクトは非表示に
            if (s.safeVertical.top == 0)
                safeTop.gameObject.SetActive(false);
            else
                safeTop.gameObject.SetActive(true);
            if (s.safeVertical.bottom == 0)
                safeBottom.gameObject.SetActive(false);
            else
                safeBottom.gameObject.SetActive(true);
            if (s.safeVertical.left == 0)
                safeLeft.gameObject.SetActive(false);
            else
                safeLeft.gameObject.SetActive(true);
            if (s.safeVertical.right == 0)
                safeRight.gameObject.SetActive(false);
            else
                safeRight.gameObject.SetActive(true);
 
        }
        else
        {
            safeTop.sizeDelta = new Vector2(0, s.safeHorizontal.top * s.retina);
            safeBottom.sizeDelta = new Vector2(0, s.safeHorizontal.bottom * s.retina);
            safeLeft.sizeDelta = new Vector2(s.safeHorizontal.left * s.retina, 0);
            safeRight.sizeDelta = new Vector2(s.safeHorizontal.right * s.retina, 0);

            //サイズが0のオブジェクトは非表示に
            if (s.safeHorizontal.top == 0)
                safeTop.gameObject.SetActive(false);
            else
                safeTop.gameObject.SetActive(true);
            if (s.safeHorizontal.bottom == 0)
                safeBottom.gameObject.SetActive(false);
            else
                safeBottom.gameObject.SetActive(true);
            if (s.safeHorizontal.left == 0)
                safeLeft.gameObject.SetActive(false);
            else
                safeLeft.gameObject.SetActive(true);
            if (s.safeHorizontal.right == 0)
                safeRight.gameObject.SetActive(false);
            else
                safeRight.gameObject.SetActive(true);
        }
        //サイズの半分だけずらす
        safeTop.anchoredPosition = new Vector2(0, (int)-safeTop.sizeDelta.y / 2);
        safeBottom.anchoredPosition = new Vector2(0, (int)safeBottom.sizeDelta.y / 2);
        safeLeft.anchoredPosition = new Vector2((int)safeLeft.sizeDelta.x / 2, 0);
        safeRight.anchoredPosition = new Vector2((int)-safeRight.sizeDelta.x / 2, 0);


    }

    void SetInvisibleArea(Safe.IOSDevice s)
    {
        Vector2 newSize = defaultSize * s.retina;

        leftTop.sizeDelta = newSize;
        leftBottom.sizeDelta = newSize;
        rightTop.sizeDelta = newSize;
        rightBottom.sizeDelta = newSize;

        //サイズの半分だけずらす
        leftTop.anchoredPosition = new Vector2((int)leftTop.sizeDelta.x / 2, (int)-leftTop.sizeDelta.y / 2);
        leftBottom.anchoredPosition = new Vector2((int)leftBottom.sizeDelta.x / 2, (int)leftBottom.sizeDelta.y / 2);
        rightTop.anchoredPosition = new Vector2((int)-rightTop.sizeDelta.x / 2, (int)-rightTop.sizeDelta.y / 2);
        rightBottom.anchoredPosition = new Vector2((int)-rightBottom.sizeDelta.x / 2, (int)rightBottom.sizeDelta.y / 2);

    }

    public Vector2 GetShift(Shift.direction dir)
    {
        if (dir == Shift.direction.Top)
            return new Vector2(0, -safeTop.sizeDelta.y);
        else if (dir == Shift.direction.Bottom)
            return new Vector2(0, safeTop.sizeDelta.y);
        else if (dir == Shift.direction.Left)
            return new Vector2(safeTop.sizeDelta.x, 0);
        else
            return new Vector2(-safeTop.sizeDelta.x, 0);
    }

    private void ChangePad()
    {
        defaultSize = new Vector2(16, 16);
        leftTop.GetComponent<Image>().sprite = leftTop_pad;
        leftBottom.GetComponent<Image>().sprite = leftBottom_pad;
        rightTop.GetComponent<Image>().sprite = rightTop_pad;
        rightBottom.GetComponent<Image>().sprite = rightBottom_pad;
    }

    public IOSDevices GetDevices()
    {
        return devices;
    }

    public void JudgeDevice()
    {
        Debug.Log("デバイス情報判断");
        var generation = Device.generation;
        if(generation == DeviceGeneration.iPhoneX)
        {
            if (isVertical)
                fix = safe.devices[(int)IOSDevices.iPhoneXorXS].rectVertical * safe.devices[(int)IOSDevices.iPhoneXorXS].retina;
            else
                fix = safe.devices[(int)IOSDevices.iPhoneXorXS].rectHorizontal * safe.devices[(int)IOSDevices.iPhoneXorXS].retina;
        }
        //XR
        else if (generation == DeviceGeneration.iPhoneUnknown && (fix == new Vector2(750, 1624) || fix == new Vector2(1624, 750)))
        {
            if (isVertical)
                fix = safe.devices[(int)IOSDevices.iPhoneXR].rectVertical * safe.devices[(int)IOSDevices.iPhoneXR].retina;
            else
                fix = safe.devices[(int)IOSDevices.iPhoneXR].rectHorizontal * safe.devices[(int)IOSDevices.iPhoneXR].retina;
        }
        //XSMax
        else if (generation == DeviceGeneration.iPadUnknown && (fix == safe.devices[(int)IOSDevices.iPhoneXSMax].rectHorizontal || fix == safe.devices[(int)IOSDevices.iPhoneXSMax].rectVertical))
        {
            if (isVertical)
                fix = safe.devices[(int)IOSDevices.iPhoneXSMax].rectVertical * safe.devices[(int)IOSDevices.iPhoneXSMax].retina;
            else
                fix = safe.devices[(int)IOSDevices.iPhoneXSMax].rectHorizontal * safe.devices[(int)IOSDevices.iPhoneXSMax].retina;
        }
        //iPadPro11
        else if(generation==DeviceGeneration.iPadUnknown &&(fix == safe.devices[(int)IOSDevices.iPadPro11Inch].rectHorizontal || fix == safe.devices[(int)IOSDevices.iPadPro11Inch].rectVertical))
        {
            if (isVertical)
                fix = safe.devices[(int)IOSDevices.iPadPro11Inch].rectVertical * safe.devices[(int)IOSDevices.iPadPro11Inch].retina;
            else
                fix = safe.devices[(int)IOSDevices.iPadPro11Inch].rectHorizontal * safe.devices[(int)IOSDevices.iPadPro11Inch].retina;
        }
        //iPadPro12.9
        else if (generation == DeviceGeneration.iPadUnknown && (fix == safe.devices[(int)IOSDevices.iPadPro12_9Inch].rectHorizontal || fix == safe.devices[(int)IOSDevices.iPadPro12_9Inch].rectVertical))
        {
            if (isVertical)
                fix = safe.devices[(int)IOSDevices.iPadPro12_9Inch].rectVertical * safe.devices[(int)IOSDevices.iPadPro12_9Inch].retina;
            else
                fix = safe.devices[(int)IOSDevices.iPadPro12_9Inch].rectHorizontal * safe.devices[(int)IOSDevices.iPadPro12_9Inch].retina;
        }

        Debug.Log(fix);
    }
}
