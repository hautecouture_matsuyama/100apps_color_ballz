﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NameLocalization : MonoBehaviour {

    //日本語テキスト
    [SerializeField]
    private Text jpText;

    //中国語。繁体簡体を含む
    [SerializeField]
    private Text cnText;
    
    //それ以外（今回は英語で統一）
    [SerializeField]
    private Text otherText;

    //表示するテキスト
    [SerializeField]
    private Text mainText;

    [SerializeField]
    private InputField inputField;

    SystemLanguage language;

    void Awake()
    {
        language = Application.systemLanguage;
        if (language == SystemLanguage.Japanese)
        {
            mainText.text = jpText.text;
            if (inputField != null)
            {
                inputField.contentType = InputField.ContentType.Standard;
                inputField.characterLimit = 5;
            }
        }
        else if (language == SystemLanguage.Chinese || language == SystemLanguage.ChineseSimplified || language == SystemLanguage.ChineseTraditional)
        {
            mainText.text = cnText.text;
            if (inputField != null)
            {
                inputField.contentType = InputField.ContentType.Standard;
                inputField.characterLimit = 5;
            }
        }
        else
        {
            mainText.text = otherText.text;
            if (inputField != null)
            {
                inputField.contentType = InputField.ContentType.Alphanumeric;
                inputField.characterLimit = 7;
            }
        }
    }
}
